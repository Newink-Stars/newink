package com.newink;

import org.jasypt.encryption.StringEncryptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ApplicationContext;

/**
 * 启动程序
 * 
 * @author newink
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
@MapperScan("com.newink.*.mapper")
public class NewInkApplication implements CommandLineRunner
{
    @Autowired(required=false)
    private ApplicationContext appCtxt;

    @Autowired(required=false)
    private StringEncryptor codeSheepEncrytorBean;

    public static void main(String[] args)
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");

        SpringApplication.run(NewInkApplication.class, args);
        System.out.println("=======================NewInk系统=======================");
        System.out.println("=======================启 动 成 功=======================");
    }

    @Override
    public void run(String... args) throws Exception {
    }

    private String encrypt(String originPassord) {
        String encryptStr = codeSheepEncrytorBean.encrypt(originPassord);
        return encryptStr;
    }

    private String decrypt(String encrytedPassword){
        String decryptStr = codeSheepEncrytorBean.decrypt(encrytedPassword);
        return decryptStr;
    }
}